def fibonacci(dlzka):
    if dlzka == 2 or dlzka == 1:
        return "Neexistuje fibonacciho rad pre dlzku 1 a 2"
    postupnost = [0, 1]
    for i in range(2, dlzka +1):
        postupnost.append(postupnost[i-2] + postupnost[i-1])
    return postupnost
dlzka = int(input("Zadaj pocet clenov, ktore chces mat: "))
print(fibonacci(dlzka))
