slovnik = {}

n = int(input("Zadaj pocet slov, ktore chces dat do slovnika. "))

with open("Slovnik.txt", "a") as subor:
    for i in range(n):
        svk = input("Zadaj slovenske slovo: ")
        eng = input("Zadaj toto slovo po anglicky: ")

        subor.write(f"{svk}={eng}\n")
def preloz(slovo):
    with open("Slovnik.txt", "r") as subor:
        for riadok in subor:
            rovnasa = riadok.find("=")
            svk = riadok[:rovnasa]
            eng = riadok[rovnasa:]

            slovnik[svk] = eng.replace("=", "")
        if slovo in slovnik.keys():
            return slovnik[slovo]
        else:
            return "Slovo nie je v slovniku"

print(preloz("mama"), end = "")

