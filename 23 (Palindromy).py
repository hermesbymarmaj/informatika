def palindromy(text):
    text = text.split()
    palindromy = []
    for slovo in text:
        slovo = slovo.strip(",")
        slovo = slovo.lower()
        
        if slovo == slovo[::-1]:
            palindromy.append(slovo)
    return palindromy

def vypis(zoznam):
    print("Palindromy: ", end= "")
    print(zoznam[0], end = "")
    for i in range(1, len(zoznam)):
        print(f", {zoznam[i]}", end = "")
vypis(palindromy("Anna, matej simon madam"))

