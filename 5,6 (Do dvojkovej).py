def doDvojkovej(cislo):
    dvojkove = ""
    while cislo != 0:
        dvojkove += str(cislo % 2)
        cislo = cislo // 2
    dvojkove = dvojkove[::-1]
    return dvojkove
def doDesiatkovej(cislo):
    n = 0
    desiatkove = 0
    cislo = str(cislo[::-1])
    for char in cislo:
        desiatkove += int(char) * (2 ** n)
        n += 1
    return desiatkove


cislo = int(input("Zadaj cislo: "))
print(doDvojkovej(cislo))
print(doDesiatkovej(doDvojkovej(cislo)))
