from random import randint

def kocky(n):
    pocet = []
    sucet = 0
    for i in range(n):
        k1= randint(1, 6)
        k2 = randint(1,6)
        pokusy = 0
        while True:
            pokusy += 1
            k1= randint(1, 6)
            k2 = randint(1,6)
            if k1 == 6 and k2 == 6:
                break
        pocet.append(pokusy)

    for i in pocet:
        sucet += i

    return sucet / n

n = int(input("Zadaj n: "))

print("Priemerny pocet pokusov na pad 2 sestiek je:", kocky(n))
