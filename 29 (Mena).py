def najmensie(zoznam):
    x = "z" * 1000
    for i in zoznam:
        if x > i:
            x = i
    return x

def zorad(zoznam):
    zoradene = []
    element = ""
    for i in range(len(zoznam)):
        element = najmensie(zoznam)
        ciselko = zoznam.index(element)
    
        zoradene.append(element)
        del zoznam[ciselko]
    return zoradene
mena = ["Matej", "Martin", "Anna", "Samuel", "Peter", "Patrik", "Simon"]
zoradene = zorad(mena)
print("Zoradene mena su: ")
for meno in zoradene:
    print(meno, end= " ")
