def naHodiny(n):

    pocetDni = n // 86400
    n -= 86400 * pocetDni
    
    pocetHodin = n // 3600
    n -= 3600*pocetHodin
    
    pocetMinut = n // 60
    n -= 60 * pocetMinut

    return f"{pocetDni} dni, {pocetHodin} hodin, {pocetMinut} minut."

print(naHodiny(int(input("Zadaj pocet sekund: "))))
