text = input("Zadaj text: ")

pocetMedzier = text.count(" ")
pocetZnakov = len(text.replace(" ", ""))
slova = text.split()
cisla = 0
for slovo in slova:
    if slovo.isdigit():
        cisla += 1
pocetSlov = len(slova)

print(f"Text má {pocetSlov} slov, {pocetZnakov} znakov, {cisla} cislic, {pocetMedzier} medzier.")
