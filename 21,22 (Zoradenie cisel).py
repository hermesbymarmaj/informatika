import random

def zorad(zoznam, zostupne):
    zoradeny = []
    for i in range(len(zoznam)):
        if zostupne == True:
            zoradeny.append(max(zoznam))
            zoznam.remove(max(zoznam))
        else:
            zoradeny.append(min(zoznam))
            zoznam.remove(min(zoznam))
    return zoradeny

def vypis(zoznam, vedlaSeba):
    print()
    print("Cisla su: ")
    if vedlaSeba == True:
        print(zoznam[0], end = "")
        for i in range(1, len(zoznam)):
            print(f", {zoznam[i]}", end = "")
        print()
    else:
        for i in range(len(zoznam)):
            print(zoznam[i])
    
    
cisla = []
cisla2 = []
for _ in range(10):
    #cislo = int(input("Zadaj cele cislo: "))
    cisla.append(random.randint(-500, 500))
    cisla2.append(random.randint(-500, 500))

zoradeneZ = zorad(cisla, True)
zoradeneV = zorad(cisla2, False)

vypis(zoradeneZ, True)
vypis(zoradeneV, False)
