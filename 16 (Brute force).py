from random import choice
cisla = [x for x in range(0,10)]
pismena = [chr(x) for x in range(97, 108)]

znaky = cisla + pismena
heslo = ""
for _ in range(5):
    heslo = heslo + str(choice(znaky))
pokusy = 0
print(heslo)
while True:
    hadaneHeslo = "".join(str(choice(znaky)) for i in range(5))
    pokusy += 1
    if heslo == hadaneHeslo:
        break

print(pokusy)

