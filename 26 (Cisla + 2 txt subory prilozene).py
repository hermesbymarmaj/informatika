def parneCislo(a):
    if not a%2:
        return True
    else:
        return False
parneCisla = []
with open("Cisla.txt", "r") as subor:
    cisla = [int(x) for x in subor]
    for cislo in cisla:
        if parneCislo(cislo):
            parneCisla.append(cislo)

with open("ParneCisla.txt", "w") as subor:
    for cislo in parneCisla:
        subor.write(f"{cislo}\n")

