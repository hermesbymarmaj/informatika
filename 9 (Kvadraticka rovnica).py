def kvadraSolver(a,b,c):
    riesenie = []
    diskriminant = b**2 - 4*a*c
    if diskriminant > 0:
      riesenie.append((-b + diskriminant ** (0.5)) / (2*a))
      riesenie.append((-b - diskriminant ** (0.5)) / (2*a))
      return riesenie
    elif diskriminant == 0:
      riesenie.append((-b + diskriminant ** (0.5)) / (2*a))
      return riesenie
    else:
      return ("Kvadra nema v obore realnych cisel riesenie!")

