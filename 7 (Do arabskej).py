legenda = {  
    1000:"M",
    900:"CM",
    500:"D",
    400:"CD",
    100:"C",
    99:"XCIX",
    90:"XC",
    50:"L",
    40:"XL",
    10:"X",
    9:"IX",
    8:"VIII",
    7:"VII",
    6:"VI",
    5:"V",
    4:"IV",
    3:"III",
    2:"II",
    1:"I",
}
legenda2 = {
    "I": 1,
    "V": 5,
    "X":10,
    "L":50,
    "C":100,
    "D":500,
    "M":1000,
    "":0
}
keys = []
for key, value in legenda.items():
    keys.append(key)



def doRimskych(cislo):
    cislaL = legenda.keys()
    rimskeCislo = ""
    if cislo in cislaL:
      return legenda[cislo]
    else:
      for delitel in keys:
        for i in range(cislo//delitel):
          rimskeCislo = rimskeCislo + str(legenda[delitel])
          cislo -= delitel
  
    return rimskeCislo
def doArabskych(cislo):
    i=0
    cislaL = legenda2.keys()
    arabske = 0
    if cislo in cislaL:
        return legenda2[cislo]
    else:
        while i < len(cislo):
            cislo1 = cislo[i]
           
            if i != len(cislo) - 1:
                cislo2 = cislo[i+1]
            else:
                cislo2=""
            
            if legenda2[cislo1] >= legenda2[cislo2]:
                arabske += int(legenda2[cislo1])
                
            else:
                ciselko = int(legenda2[cislo2]) - int(legenda2[cislo1])
                arabske += ciselko
                
                i+=1
            i += 1
    return arabske
#cislo = int(input("Zadaj cislo, ktore chces dat do rimskych: "))
#print (doRimskych(cislo))
for i in range(40):
    print(doArabskych(input("Zadaj rimske cislo: ")))

