def euklidov(a,b):
    while True:
        if a < b:
            a, b = b, a
        a -= b
        if b == 0:
            break
    return a

cislo1 = int(input("Zadaj prve cislo: "))
cislo2 = int(input("Zadaj druhe cislo: "))

def euklidov2(a,b):
    if a < b:
        a, b = b, a
    while a % b:
        a, b = b, a % b
    return b

print(euklidov(cislo1, cislo2))
print(euklidov2(cislo1, cislo2))
